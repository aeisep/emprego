// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import Buefy from 'buefy';
import 'buefy/dist/buefy.css'
import Moment from 'moment';
import VueMoment from 'vue-moment';
import VueClipboard from 'vue-clipboard2';
import vuetify from '@/plugins/vuetify';
import App from './App';
import router from './router';

Moment.locale('pt');

Vue.use(Buefy);
Vue.use(VueMoment, Moment);
Vue.use(VueClipboard);

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  vuetify,
  template: '<App/>',
  components: { App },
});
