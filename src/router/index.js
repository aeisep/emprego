import Vue from 'vue';
import Router from 'vue-router';
import OffersComponent from '@/components/OffersComponent';
import OfferComponent from '@/components/OfferComponent';
import RegisterComponent from '@/components/RegisterComponent';

Vue.use(Router);

export default new Router({
  routes: [
    {
      path: '/',
      name: 'offers',
      component: OffersComponent,
    },
    {
      path: '/offer/:offerId',
      name: 'offer',
      component: OfferComponent,
      props: true,
    },
    // comment when uploading to production
    // we don't want users accessing this incomplete
    // component
     /* {
      path: '/register',
      name: 'register',
      component: RegisterComponent,
    } */
  ],
});
